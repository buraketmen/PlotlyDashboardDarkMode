def zerotofivehundred(x):
    if x <=50:
        return "0-50"
    if x <=100:
        return "50-100"
    if x <= 150:
        return "100-150"
    if x <= 200:
        return "150-200"
    if x <= 250:
        return "200-250"
    if x <= 300:
        return "250-300"
    if x <= 350:
        return "300-350"
    if x <= 400:
        return "350-400"
    if x <= 450:
        return "400-450"
    if x<=500:
        return "450-500"

def zerotoonehundred(x):
    if x<=10:
        return "0-10"
    if x<=20:
        return "10-20"
    if x<=30:
        return "20-30"
    if x<=40:
        return "30-40"
    if x<=50:
        return "40-50"
    if x<=60:
        return "50-60"
    if x<=70:
        return "60-70"

def zerototen(x):
    if x<=1:
        return "0-1"
    if x<=2:
        return "1-2"
    if x<=3:
        return "2-3"
    if x<=4:
        return "3-4"
    if x<=5:
        return "4-5"
    if x<=6:
        return "5-6"
    if x<=7:
        return "6-7"
    if x<=8:
        return "7-8"
    if x<=9:
        return "8-9"
    if x<=10:
        return "9-10"


