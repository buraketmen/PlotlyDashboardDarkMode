import pandas as pd
import numpy as np
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import plotly.graph_objs as go
import plotly.figure_factory as ff
from sklearn import preprocessing
import makeituseful as miu
from elasticsearch import Elasticsearch
import json
import requests
from dash.exceptions import PreventUpdate
from elasticsearch import helpers
import dash_daq as daq
import dash_bootstrap_components as dbc
import datetime
graph_feature = {
    'background': 'rgba(0,0,0,0)',
    'text': '#FFFFFF'}

loadingtype = ['graph', 'cube', 'circle', 'dot', 'default']
loatype = 'cube'
app = dash.Dash(__name__)
app.css.config.serve_locally = True
app.scripts.config.serve_locally = True
server = app.server
app.renderer = '''
var renderer = new DashRenderer({
    request_pre: (payload) => {
        // print out payload parameter
        console.log(payload);
    },
    request_post: (payload, response) => {
        // print out payload and response parameter
        console.log(payload);
        console.log(response);
    }
})
'''
df = pd.read_csv('ChurnTelecoms.csv', sep=',', low_memory=False)
numeric_features = None
features = None
old_df = None
df_for_feature = None
corr = None
pie_features = None
df_pie = None
df_row_count = None
time = datetime.datetime.now()
hour = time.hour
minute = time.minute
second = time.second
if(len(str(hour))==1):
    hour='0'+str(hour)
if(len(str(minute))==1):
    minute='0'+str(minute)
if(len(str(second))==1):
    second='0'+str(second)
timehourminute = "{}:{}:{}".format(hour,minute,second)
def data_manipulation():
    global df, numeric_features, features, old_df, df_for_feature, corr, pie_features, df_pie, df_row_count
    df.drop(['phone number'], axis=1, inplace=True)
    df_row_count = df.shape[0]
    old_df = df.copy()
    features = df.columns
    numerics = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']
    newdf = df.select_dtypes(include=numerics)
    numeric_features = newdf.columns
    df_pie = df.copy()
    df['total minutes'] = df['total day minutes'] + df['total eve minutes'] + df['total night minutes'] + df['total intl minutes']
    df['total calls'] = df['total day calls'] + df['total eve calls'] + df['total night calls'] + df['total intl calls']
    df['total charge'] = df['total day charge'] + df['total eve charge'] + df['total night charge'] + df['total intl charge']
    df['international plan'] = df['international plan'].map({'yes': 1, 'no': 0})
    df['voice mail plan'] = df['voice mail plan'].map({'yes': 1, 'no': 0})
    state_list = df['state'].unique().tolist()
    state_mapping = dict(zip(state_list, range(len(state_list))))
    df.replace({'state': state_mapping}, inplace=True)
    new_df = df.filter(['churn', 'state', 'customer service calls', 'total minutes', 'total calls', 'total charge', 'account length',
                        'area code', 'international plan', 'voice mail plan', 'number vmail messages'], axis=1)
    newdata = new_df.values
    columns = new_df.columns
    min_max_scaler = preprocessing.MinMaxScaler()
    newdata_scaled = min_max_scaler.fit_transform(newdata)
    df_for_feature = pd.DataFrame(data=newdata_scaled, columns=columns)
    df_for_feature['churn'] = new_df['churn']
    corr = df_for_feature.corr()
    df_pie['account length'] = df_pie['account length'].apply(miu.zerotofivehundred)
    df_pie['number vmail messages'] = df_pie['number vmail messages'].apply(miu.zerotoonehundred)
    df_pie['total day minutes'] = df_pie['total day minutes'].apply(miu.zerotofivehundred)
    df_pie['total eve minutes'] = df_pie['total eve minutes'].apply(miu.zerotofivehundred)
    df_pie['total night minutes'] = df_pie['total night minutes'].apply(miu.zerotofivehundred)
    df_pie['total intl minutes'] = df_pie['total intl minutes'].apply(miu.zerotoonehundred)
    df_pie['total day calls'] = df_pie['total day calls'].apply(miu.zerotofivehundred)
    df_pie['total eve calls'] = df_pie['total eve calls'].apply(miu.zerotofivehundred)
    df_pie['total night calls'] = df_pie['total night calls'].apply(miu.zerotofivehundred)
    df_pie['total intl calls'] = df_pie['total intl calls'].apply(miu.zerotoonehundred)
    df_pie['total day charge'] = df_pie['total day charge'].apply(miu.zerotoonehundred)
    df_pie['total eve charge'] = df_pie['total eve charge'].apply(miu.zerotoonehundred)
    df_pie['total night charge'] = df_pie['total night charge'].apply(miu.zerotoonehundred)
    df_pie['total intl charge'] = df_pie['total intl charge'].apply(miu.zerototen)

data_manipulation()

app.layout = html.Div([
    html.Div([
        html.Div([
            html.H5("CHURN IN TELECOM'S"),
            html.H6("Burak Ketmen")
        ],id="banner-text"),
        html.Div([
            #html.Img(id="logo", src="https://i.hizliresim.com/86aO61.png")
        ],id="banner-logo")
    ],id="banner",className="banner"),
    html.Pre(
        id='counter-text'),
    dcc.Interval(
        id='interval-component',
        #interval=900000,  # 900000 milliseconds = 15 min
        interval=15000,
        n_intervals=0
    ),
    html.Div([
        html.Div([
            html.Div([
                html.Div([
                    html.Div([
                        html.P("CARD 1")
                    ],id="card-1",className="one column"),
                    html.Div([
                        html.P("Number of Records"),
                        html.Div([
                            daq.LEDDisplay(
                                id='df-row-count',
                                value=df_row_count,
                                color= graph_feature['text'],
                                backgroundColor=graph_feature['background']
                            )
                        ],id="number-of-records")
                    ],id="card-2",className="three columns"),
                    html.Div([
                        html.P("Last Update Time"),
                        html.Div([
                            daq.LEDDisplay(
                                id='update-time',
                                value=timehourminute,
                                color= '#0CFF00',
                                backgroundColor=graph_feature['background']
                            )
                        ],id="Update-date")
                    ],id="card-3",className="three columns"),
                    html.Div([
                        html.P("CARD 4"),
                    ],id="card-4",className="three columns"),
                ],id="quick-stats"),
                html.Div([
                    html.Div([
                        html.Div([
                            html.Div(["Features About Churn"],className="section-banner"),
                            html.Div([
                                dcc.Dropdown(
                                    id='xaxis-histogram',
                                    className="dropdown",
                                    options=[{'label': i.title(), 'value': i} for i in features],
                                    value=features[0],
                                    clearable=False
                                ),
                                dcc.Loading(id="loading-histogram",
                                            children=[html.Div(dcc.Graph(id='histogram-graph', config={'toImageButtonOptions':
                                                                                                           {'width': 800,'height': 600,
                                                                                                            'format': 'png',
                                                                                                            'filename': 'histogram'}}))],type=loatype)
                            ],id="histogram")
                        ],id="histogram-outer"),
                        html.Div([
                            html.Div(["Heatmap"],className="section-banner"),
                            html.Div([
                                dcc.Loading(id="loading-heatmap",
                                            children=[html.Div([
                                                dcc.Graph(id='heatmap-graph', config={'toImageButtonOptions':
                                                                                          {'width': 1000,
                                                                                           'height': 1000,
                                                                                           'format': 'png',
                                                                                           'filename': 'heatmap'}})],id="heatmap-children")],
                                            type=loatype)
                            ],id="heatmap")
                        ],id="heatmap-outer"),
                        html.Div([
                            html.Div(["Box Plot"],className="section-banner"),
                            dcc.Dropdown(
                                id='axis-box',
                                className="dropdown",
                                options=[{'label': i.title(), 'value': i} for i in numeric_features],
                                value=numeric_features[0]
                            ),
                            dcc.Loading(id="loading-box",
                                        children=[html.Div(
                                            dcc.Graph(id='box-graph', config={'toImageButtonOptions':
                                                                                    {'width': 1500,
                                                                                     'height': 1300,
                                                                                     'format': 'png',
                                                                                     'filename': 'box'}}))], type=loatype)
                        ],id='box-outer')
                    ],id="top-section-container"),
                    html.Div([

                        html.Div([
                            html.Div(["Map Chart"],className="section-banner"),
                            dcc.Dropdown(
                                id='axis-map',
                                className="dropdown",
                                options=[{'label': i.title(), 'value': i} for i in numeric_features],
                                value=numeric_features[0],
                                clearable=False
                            ),
                            dcc.Loading(id="loading-map",
                                        children=[html.Div([
                                            dcc.Graph(id='map-graph', config={'toImageButtonOptions':
                                                                                  {'width': 1500,
                                                                                   'height': 800,
                                                                                   'format': 'png',
                                                                                   'filename': 'map'}})],id="map-children")],type=loatype)
                        ],id="map-outer",className="five columns"),
                        html.Div([
                            html.Div(["Donut Charts"],className="section-banner"),
                            dcc.Dropdown(
                                id='axis-pie',
                                className="dropdown",
                                options=[{'label': i.title(), 'value': i} for i in features],
                                value=features[1:3],
                                multi=True
                            ),
                            html.Div([
                                dcc.Loading(id="loading-pie",
                                            children=html.Div(id='pie-graph'), className="row", type=loatype)
                            ],id="pie")
                        ],id='pie-outer',className='seven columns')
                    ],id="medium-chart-container",className="row"),
                    html.Div([

                    ],id="bottom-chart-container",className="twelve columns")
                ],id="graphs-container")
            ],id="status-container")
        ],id="app-content")
    ],id="app-container")
], id="big-app-container")

@app.callback(
    Output('histogram-graph', 'figure'),
    [Input('xaxis-histogram', 'value'), Input('interval-component', 'n_intervals')])
def update_histogram(xaxis_name,n):
    if xaxis_name is None:
        raise PreventUpdate
    traces = []
    for churn_name in old_df['churn'].unique().tolist():
        df_by_churn = old_df[old_df['churn'] == churn_name]
        traces.append(go.Histogram(
            x=df_by_churn[xaxis_name],
            opacity=1,
            name=churn_name
        ))
    return {
        'data': traces,
        'layout': go.Layout(
            margin={'l': 75, 'b': 50, 't': 50, 'r': 75},
            #hovermode='closest',
            # barmode = 'overlay',
            xaxis={'title': xaxis_name.title()},
            yaxis={'title': 'Count'},
            plot_bgcolor= graph_feature['background'],
            paper_bgcolor=graph_feature['background'],
            font={'color':graph_feature['text']}
        )
    }

@app.callback(
    Output('heatmap-graph', 'figure'),
    [Input('interval-component', 'n_intervals')])
def update_heatmap(n):
    return {
        "data": [go.Surface(x=df_for_feature.columns, y=df_for_feature.columns, z=corr.values.tolist(),
                            colorscale='Viridis', showscale=True)],
        "layout": go.Layout(
            plot_bgcolor=graph_feature['background'],
            paper_bgcolor=graph_feature['background'],
            font={'color': graph_feature['text']}
        )
    }

@app.callback(
    Output('box-graph', 'figure'),
    [Input('axis-box', 'value'), Input('interval-component', 'n_intervals')])
def update_histogram(xaxis_name,n):
    traces = []
    for churn_name in df['churn'].unique().tolist():
        df_by_churn = df[df['churn'] == churn_name]
        traces.append(go.Box(
            y=df_by_churn[xaxis_name],
            opacity=1,
            name=churn_name
        ))
    return {
        'data': traces,
        'layout': go.Layout(
            plot_bgcolor=graph_feature['background'],
            paper_bgcolor=graph_feature['background'],
            font={'color': graph_feature['text']}
        )
    }
@app.callback(
    Output('map-graph', 'figure'),
    [Input('axis-map', 'value'), Input('interval-component', 'n_intervals')])
def update_map(axis_name,n):
    if axis_name is None:
        raise PreventUpdate
    country = {"AL": "Alabama","AK": "Alaska","AS": "American Samoa","AZ": "Arizona","AR": "Arkansas","CA": "California",
               "CO": "Colorado","CT": "Connecticut","DE": "Delaware","DC": "District Of Columbia","FM": "Federated States Of Micronesia","FL": "Florida",
               "GA": "Georgia","GU": "Guam","HI": "Hawaii","ID": "Idaho","IL": "Illinois","IN": "Indiana","IA": "Iowa","KS": "Kansas",
               "KY": "Kentucky","LA": "Louisiana","ME": "Maine","MH": "Marshall Islands","MD": "Maryland","MA": "Massachusetts","MI": "Michigan",
               "MN": "Minnesota","MS": "Mississippi","MO": "Missouri","MT": "Montana","NE": "Nebraska","NV": "Nevada","NH": "New Hampshire","NJ": "New Jersey","NM": "New Mexico",
               "NY": "New York","NC": "North Carolina","ND": "North Dakota","MP": "Northern Mariana Islands","OH": "Ohio","OK": "Oklahoma","OR": "Oregon",
               "PW": "Palau","PA": "Pennsylvania","PR": "Puerto Rico","RI": "Rhode Island","SC": "South Carolina","SD": "South Dakota",
               "TN": "Tennessee","TX": "Texas","UT": "Utah","VT": "Vermont","VI": "Virgin Islands","VA": "Virginia","WA": "Washington",
               "WV": "West Virginia","WI": "Wisconsin","WY": "Wyoming"}

    dff = df.groupby(['state']).mean().reset_index()

    dff['state'] =old_df['state'].unique()
    dff['fullname'] = dff['state'].map(country)
    dff['text'] = dff['fullname'] + '<br>'

    return {
        'data': [go.Choropleth(locations= dff['state'], z = dff[axis_name], locationmode='USA-states', text= dff['text'],
                               colorbar={'title': {"text": "Average", "side": "top"}})],
        'layout': go.Layout(
            margin={'l': 20, 'b': 25, 't': 25, 'r': 100},
            geo={'scope':'usa', 'bgcolor':graph_feature['background']},
            plot_bgcolor=graph_feature['background'],
            paper_bgcolor=graph_feature['background'],
            font={'color': graph_feature['text']}
        )
    }

@app.callback(
    Output('pie-graph', 'children'),
    [Input('axis-pie', 'value'), Input('interval-component', 'n_intervals')])
def update_pie(axis_pie,n):
    axis_pie = list(axis_pie)
    graphs= []
    if len(axis_pie) > 2:
        class_choice = 'four columns'
    elif len(axis_pie) == 2:
        class_choice = 'six columns'
    else:
        class_choice = 'twelve columns'

    for axis in axis_pie:
        data = go.Pie(labels=df_pie[axis].unique().tolist(),values=df_pie.groupby([axis]).size().tolist(),hole=.4)
        graphs.append(html.Div(dcc.Graph(
            id=axis,
            animate=True,
            figure={'data': [data],
                    'layout' : go.Layout(margin={'l':50,'r':50,'t':50,'b':25},
                                         title='{}'.format(axis.title()),
                                         plot_bgcolor=graph_feature['background'],
                                         paper_bgcolor=graph_feature['background'],
                                         font={'color': graph_feature['text']}
                                         )},), className=class_choice))
    return graphs

@app.callback(Output('update-time', 'value'),
              [Input('interval-component', 'n_intervals')])
def update_data(n):
    global timehourminute
    time = datetime.datetime.now()
    hour = time.hour
    minute = time.minute
    second = time.second
    if(len(str(hour))==1):
        hour='0'+str(hour)
    if(len(str(minute))==1):
        minute='0'+str(minute)
    if(len(str(second))==1):
        second='0'+str(second)
    timehourminute = "{}:{}:{}".format(hour,minute,second)
    return timehourminute

@app.callback(Output('counter-text', 'children'),
              [Input('interval-component', 'n_intervals')])
def update_data(n):
    global df
    df = pd.read_csv('ChurnTelecoms.csv', sep=',', low_memory=False)
    data_manipulation()

if __name__ == '__main__':
    app.run_server(debug=True,port=8060)